#问题：1，顾问和经理是不是同一个人？

#数据库
CREATE DATABASE IF NOT EXISTS cmgr;

#用户表（经理表）
CREATE TABLE IF NOT EXISTS manager(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  username VARCHAR(20) UNIQUE NOT NULL,#用户名
  password VARCHAR(20) NOT NULL ,#密码
  name VARCHAR(20) NOT NULL, #真实姓名
  phone CHAR(11) UNIQUE NOT NULL ,#手机号码
  role TINYINT(1) DEFAULT 0,# 用户角色：0代表普通用户， 1代表超级管理员用户
  status TINYINT(1) DEFAULT 0, #用户状态：0代表激活，1代表未激活
  datetime INT(10) NOT NULL
);

#客户表
CREATE TABLE IF NOT EXISTS guest(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  name VARCHAR(20) UNIQUE NOT NULL,#姓名
  gender TINYINT(1) DEFAULT 0, #性别：0代表女，1代表男 
  phone CHAR(11) UNIQUE NOT NULL ,#手机号码
  level TINYINT(1) DEFAULT 0, #客户级别：0：高，1：中，2：低
  source TINYINT(1) DEFAULT 0, #客户来源，0代表其他，1代表网络， 2代表外拓 ...
  interest TINYINT(10) NOT NULL,#意向：0:高， 1:中，2：低 
  status TINYINT(10) NOT NULL, #0：已定未交，1：保有，2：战败结束，3：退订，4：退车，5：待完善，
  remark VARCHAR(200) DEFAULT '',#备注
  
  manager_id INT(10) NOT NULL, #创建人id,
  car_id INT(10) NOT NULL,#汽车id
  
  datetime INT(10) NOT NULL#登记日期
);

#汽车表
CREATE TABLE IF NOT EXISTS car(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  name VARCHAR(20) NOT NULL,#汽车名字
  brand_id INT(10) NOT NULL,#汽车品牌id
  model_id INT(10) NOT NULL,#汽车型号id
  style_id INT(10) NOT NULL,#汽车款型id
  datetime INT(10) NOT NULL #添加日期
);

#汽车品牌表
CREATE TABLE IF NOT EXISTS brand(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  name VARCHAR(30) NOT NULL UNIQUE,
);

#汽车型号表:汽车型号从属于一个既定的汽车品牌
CREATE TABLE IF NOT EXISTS model(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  name VARCHAR(30) NOT NULL,
  brand_id INT(10) NOT NULL#品牌id
);

#汽车款型表：尊贵，豪华等等
CREATE TABLE IF NOT EXISTS style(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  name VARCHAR(30) NOT NULL UNIQUE
);